/*
 * App.js
 ******************************/
const
  express = require('express'),
  app = express(),
  expressSession = require('express-session'),
  mongoose = require('mongoose'),
  mongoStore = require('connect-mongo'),
  cookieParser = require('cookie-parser'),
  MongoStore = mongoStore(expressSession),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  // passport = require('passport'),
  port = process.env.PORT || 8000

// Connect Mongoose with MongoDB
// const urlDb = 'mongodb://localhost:27017/portfolio'
const urlDb = 'mongodb+srv://drk:drk@cluster0-4asdq.mongodb.net/test?retryWrites=true&w=majority'

// app.use('*', (req, res, next) => {
//   // res.header('Access-Control-Allow-Origin', '*')
//   // res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
//   console.log(req.session)
//   next()
// })

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.header('origin'))
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

// Cors
app.use(cors({
  origin: ['http://localhost:8000'],
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  credentials: true
}))

// Body-Parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// Express-session// Mongoose
mongoose
  .connect(urlDb, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => console.log('Connecter a MongoDB'))
  .catch(err => console.log(err))

app.use(expressSession({
  secret: 'securite',
  name: 'DrQSs',
  secure: false,
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  })
}))

// cookieParser should be above session
app.use(cookieParser())

// app.use
app.use(express.static('assets'))

// ROUTER
const ROUTER = require('./controller/router')
app.use('/api', ROUTER)

// Page Err 404
app.use((req, res) => {
  res.send('err404')
})

// Ecoute de notre app
app.listen(port, () => {
  console.log('le serveur tourne sur le port: ' + port)
})
