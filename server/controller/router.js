const
  express = require('express'),
  router = express.Router()

// Import Controller
const
  ArticleController = require('./article/ArticleController'),
  MpController = require('./mp/MpController'),
  AuthController = require('./auth/AuthController'),
  SessionController = require('./auth/SessionController')

// Import Middleware

// Routes Article
router.route('/article')
  .get(ArticleController.get)
  .post(ArticleController.post)

router.route('/article/:id')
  .put(ArticleController.put)
  .delete(ArticleController.deleteOne)

// Routes Mp
router.route('/mp')
  .get(MpController.get)
  .post(MpController.post)

router.route('/mp/:id')
  .delete(MpController.deleteOne)

// Route Auth
router.route('/register')
  .post(AuthController.register)

router.route('/login')
  .post(AuthController.login)

router.route('/logout')
  .get(AuthController.logout)

router.route('/session')
  .get(SessionController.get)

module.exports = router
