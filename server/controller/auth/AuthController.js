/*
 * Article Controller
 *********************/
const
  User = require('../../model/User'),
  bcrypt = require('bcrypt'),
  jwt = require('jsonwebtoken')

module.exports = {
  get: async (req, res) => {
    const dbUser = await User.find({})
    res.json(dbUser)
  },
  register: async (req, res, next) => {
    // const dbUser = await User.find({})
    console.log(req.body)
    User.create({
      ...req.body
    })
    next()
  },
  login: async (req, res) => {
    let userAuth = await User.findOne({ email: req.body.email })
    // const dbUserID = await User.find({})

    if (!userAuth) {
      console.log('pas ds la db')
      res.redirect('/login')
    } else {
      const
        { email, password } = req.body
        // , dbUser = await User.find({})
        // L'on déclare la récupération de notre req.session pour pouvoir lui attribuer notre function
        // Par la suite
        // eslint-disable-next-line comma-style
        , sess = req.session

      console.log(req.body)

      User.findOne({ email }, (err, User) => {
        if (err) {
          console.log(err)

        // eslint-disable-next-line padded-blocks
        } else if (User) {
          // eslint-disable-next-line padded-blocks
          bcrypt.compare(password, User.password, (err, same) => {

            if (err) {
              console.log(err)

            // eslint-disable-next-line padded-blocks
            } else if (same) {
              const payload = {
                _id: User._id,
                pseudo: User.pseudo,
                email: User.email
              }
              let token = jwt.sign(payload, 'token', {
                expiresIn: 1440
              })
              console.log('OK 1')
              sess.email = User.email
              sess.status = User.status
              sess.pseudo = User.pseudo
              sess.isVerified = User.isVerified
              sess.imgUser = User.imgUser
              sess.userId = User._id
              sess.isAdmin = User.isAdmin
              sess.isModo = User.isModo
              // sess.token = token
              console.log(req.session)

              res.send({
                sess, token
              })
            }
          })
        } else {
          console.log(err)
        }
      })
    }
  },
  logout: (req, res) => {
    const sess = req.session
    req.session.destroy(() => {
      res.clearCookie(`DrQSess`)
      console.log(req.session)
      res.send({
        sess
      })
    })
  }
}
