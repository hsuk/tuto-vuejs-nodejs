/*
 * MP Controller
 ***************/
const
  Mp = require('../../model/message')

module.exports = {
  get: async (req, res) => {
    const dbMp = await Mp.find({})
    console.log(dbMp)
    res.json(dbMp)
  },
  post: async (req, res, next) => {
    console.log(req.body)
    Mp.create({
      ...req.body
    })
    next()
  },
  deleteOne: async (req, res, next) => {
    const dbMp = await Mp.find({})
    Mp.deleteOne({
      _id: req.params.id
    },
    (err) => {
      if (!err) {
        res.json(dbMp)
      } else {
        res.send(err)
      }
    })
  }
}
