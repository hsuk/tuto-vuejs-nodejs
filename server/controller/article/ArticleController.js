/*
 * Article Controller
 *********************/
const
  Article = require('../../model/ArticleBlog')

module.exports = {
  get: async (req, res) => {
    const dbArticle = await Article.find({})
    console.log(req.session)
    res.json(dbArticle)
  },
  post: async (req, res, next) => {
    const dbMp = await Article.find({})
    console.log(req.body)
    Article.create({
      ...req.body
    })
    res.json(dbMp)
  },
  put: async (req, res, next) => {
    const dbArticle = await Article.findById(req.params.id)
    let query = { _id: req.params.id }
    Article.findOneAndUpdate(
      query,
      {
        title: req.body.title,
        author: req.body.author,
        message: req.body.message
      },
      { useFindAndModify: false },
      function (err, post) {
        if (err) {
          console.log('Err: ' + err)
          res.redirect('/')
        } else {
          console.log(req.body)
          console.log('Modification OK')
          res.json(dbArticle)
        }
      }
    )
  },
  deleteOne: async (req, res, next) => {
    const dbMp = await Article.find({})
    Article.deleteOne({
      _id: req.params.id
    },
    (err) => {
      if (!err) {
        res.json(dbMp)
      } else {
        res.send(err)
      }
    })
  }
}
