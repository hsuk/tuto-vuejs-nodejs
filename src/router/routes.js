
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'Home', component: () => import('pages/Index.vue') },
      { path: '/actu', name: 'Actu', component: () => import('pages/Actu.vue') },
      { path: '/projet', name: 'Projet', component: () => import('pages/Projet.vue') },
      { path: '/blog', name: 'Blog', component: () => import('pages/Blog.vue') },
      { path: '/login', name: 'Login', component: () => import('pages/Login.vue') },
      { path: '/contact', name: 'Contact', component: () => import('pages/Contact.vue') }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      { path: '', name: 'Admin', component: () => import('pages/Admin.vue') },
      { path: '/message', name: 'Message', component: () => import('pages/admin/MessageAdmin.vue') },
      { path: '/adminBlog', name: 'AdminBlog', component: () => import('pages/admin/BlogAdmin.vue') },
      { path: '/adminActu', name: 'AdminActu', component: () => import('pages/admin/ActuAdmin.vue') },
      { path: '/adminProjet', name: 'AdminProjet', component: () => import('pages/admin/ProjetAdmin.vue') }

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
