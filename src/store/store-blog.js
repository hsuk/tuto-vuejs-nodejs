import axios from 'axios'

const state = {
  dbArticle: []
}

const mutations = {
  setDbArticle (state, value) {
    state.dbArticle = value
  }
}

const actions = {
  getArticle ({ commit }) {
    axios
      .get('/article')
      .then((res) => {
        this.dbArticle = res.data
        commit('setDbArticle', this.dbArticle)
      })
  },
  // eslint-disable-next-line no-empty-pattern
  createArticle ({}, payload) {
    axios
      .post('/article', {
        author: payload.author,
        title: payload.title,
        message: payload.message
      })
      .then(_ => {
        console.log('create')
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // eslint-disable-next-line no-empty-pattern
  editOneArticle ({}, payload) {
    axios
      .put(`/article/${payload._id}`, {
        author: payload.author,
        title: payload.title,
        message: payload.message
      })
      .then(_ => {
        console.log('edit article')
      })
      .catch((error) => {
        console.log(error)
      })
  },
  // eslint-disable-next-line no-empty-pattern
  deleteOneArticle ({}, payload) {
    axios
      .delete(`/article/${payload}`)
      .then(_ => {
        console.log('Delete One Article: ' + payload)
      })
  }
}

const getters = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
