/**
 * Store Auth
 *
 *************/

import axios from 'axios'

const state = {
  loggedIn: false,
  adminIn: false,
  verifiedIn: false,
  token: null
}

const mutations = {
  setLoggedIn (state, value) {
    state.loggedIn = value
  },
  setAdminIn (state, value) {
    state.adminIn = value
  },
  setVerifiedIn (state, value) {
    state.verifiedIn = value
  }
}

const actions = {
  // eslint-disable-next-line no-empty-pattern
  registerUser ({}, payload) {
    console.log('Auth Register')
    console.log(this.formRegister)
    axios
      .post('/register', {
        pseudo: payload.pseudo,
        email: payload.email,
        password: payload.password
      })
      .then((res) => {
        console.log(res.payload)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  // eslint-disable-next-line no-empty-pattern
  loginUser ({}, payload) {
    console.log('Auth Login')
    axios
      .post('/login', {
        email: payload.email,
        pseudo: payload.pseudo,
        password: payload.password
      })
      .then((res) => {
        console.log(res)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  checkAdmin (res) {
    console.log('Check Admin')
    if (state.adminIn === false) {
      console.log('check Admin false')
      this.$router.replace('/')
    } else if (state.loggedIn === true) {
      console.log('check Admin loggedIn true')
      // this.$router.replace('/')
    } else {
      console.log('check Admin null or undefined')
      this.$router.replace('/')
    }
    console.log(state)
  },
  logoutUser ({ commit }) {
    console.log('Auth Logout')
    axios
      .get('/logout')
      .then((res) => {
        localStorage.removeItem('sess', null)
        commit('setLoggedIn', false)
        commit('setAdminIn', false)
        console.log('session User')
        this.$router.replace('/')
      })
  },
  handleAuthStateChange ({ commit }) {
    axios
      .get('/session')
      .then((res) => {
        const sess = res.data.sess
        console.log(sess)

        if (sess.status === 'user') {
          console.log('sess user')
          commit('setLoggedIn', true)

          if (sess.isVerified === true) {
            console.log('sess user')
            commit('setVerifiedIn', true)

            if (sess.isAdmin === true) {
              commit('setLoggedIn', true)
              commit('setAdminIn', true)
              console.log('session Admin')
            }
          } else {
            console.log('test session')
            commit('setLoggedIn', false)
          }
        }
      })
  }
}

const getters = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
