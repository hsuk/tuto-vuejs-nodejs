import axios from 'axios'
// let baseURL

// if (!process.env.NODE_ENV || process.env.NODE_ENV === 'dev') {
//   baseURL = 'http://127.0.0.1:8000/api'
// } else {
//   baseURL = 'https://hsuk.tk/api'
// }

export default ({ Vue }) => {
  // axios.defaults.baseURL = 'http://127.0.0.1:8000/api'
  axios.defaults.baseURL = 'https://portfolio.drkush.tk/api'
  axios.defaults.withCredentials = true
  Vue.prototype.$axios = axios
}
